const inputButton = document.querySelector('#input-keyword')
const searchButton = document.querySelector('#search-button')
searchButton.addEventListener('click', function() {
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${inputButton.value}&appid=cd0b419d2fd7aca47d46f54bd7483521&units=metric`)
        .then((ress) => ress.json())
        .then((ress) => function() {
            const result = document.querySelector('.result')

            result.innerHTML =  `<h2 style="font-size: 40px; background-color: rgb(27, 27, 126); max-width: 150px; color: white;">${ress.name}</h2>`
        })
})